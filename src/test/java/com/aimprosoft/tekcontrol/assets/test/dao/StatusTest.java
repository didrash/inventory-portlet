package com.aimprosoft.tekcontrol.assets.test.dao;

import com.aimprosoft.tekcontrol.assets.dao.StatusDAO;
import com.aimprosoft.tekcontrol.assets.domain.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

@ContextConfiguration(locations = {"classpath:spring-test-config.xml" , "classpath:spring-dataSource.xml" })
@PropertySource("classpath:jdbc.properties")
@Transactional
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@Test(groups = {"functest"}, enabled = true)

public class StatusTest extends AbstractTransactionalTestNGSpringContextTests {

    private List<Status> usedStatuses = new ArrayList<Status>();

    @Autowired
    private StatusDAO statusDAO;

    @AfterClass
    public void cleanDataBase() {

    }

    @AfterClass
    public static void cleanDataBase2() {

    }

    @Test(groups = {"odd"})
    @Parameters({ "statusTestName", "statusTestId" })
    public void TestIsNew(String statusTestName, Integer statusTestId) {
        Status status = new Status();
        status.setName(statusTestName);
        status.isNew();
        Assert.assertTrue(status.isNew(), "isNew doesn't work correctly");
        status.setId(statusTestId);
        Assert.assertFalse(status.isNew(), "isNew doesn't work correctly 2 ");
    }

    @Test(groups = {"odd"})
    @Parameters({ "statusTestName" })
    @Rollback(true)
    public void TestForCreating(String statusTestName){
        Status status = new Status();
        status.setName(statusTestName);
        statusDAO.create(status);
        Integer id = status.getId();
        Status statusFromDB = statusDAO.getById(id);
        System.out.println("statusFromDB = " + statusFromDB.toString());
        Assert.assertEquals(statusFromDB.getName(),status.getName(), "Cant save correctly name of status");
    }

}

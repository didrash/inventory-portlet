package com.aimprosoft.tekcontrol.assets.controller;

import com.aimprosoft.tekcontrol.assets.service.InventoryService;
import com.aimprosoft.tekcontrol.assets.service.OrganizationService;
import com.aimprosoft.tekcontrol.assets.service.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

@Controller
@RequestMapping(value = "VIEW")
public class IndexController {

    // Services
    @Autowired
    protected OrganizationService organizationService;
    @Autowired
    protected StatusService statusService;
    @Autowired
    protected InventoryService inventoryService;

    @RenderMapping
    public ModelAndView home(RenderRequest request, RenderResponse response) {
        ModelAndView mv = new ModelAndView("home");
        return mv;
    }

}

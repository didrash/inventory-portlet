package com.aimprosoft.tekcontrol.assets.controller;

import com.aimprosoft.tekcontrol.assets.domain.Inventory;
import com.aimprosoft.tekcontrol.assets.domain.Status;
import com.aimprosoft.tekcontrol.assets.dto.OrganizationDTO;
import com.aimprosoft.tekcontrol.assets.service.*;
import com.aimprosoft.tekcontrol.assets.service.editor.StatusEditor;
import com.aimprosoft.tekcontrol.assets.service.validator.InventoryInfoValidator;
import com.aimprosoft.tekcontrol.assets.util.SearchContainerHelper;
import com.google.gson.Gson;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.Organization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.json.MappingJacksonJsonView;

import javax.portlet.*;
import javax.servlet.http.Cookie;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.aimprosoft.tekcontrol.assets.util.Constants.*;

@Controller
@RequestMapping(value = "VIEW")
public class InventoryController {

    // Services
    @Autowired
    protected InventoryService inventoryService;
    @Autowired
    protected OrganizationService organizationService;
    @Autowired
    protected UserService userService;
    @Autowired
    protected StatusService statusService;
    @Autowired
    protected SearchContainerHelper searchContainerHelper;

    // Validators
    @Autowired
    private InventoryInfoValidator inventoryInfoValidator;

    public ModelAndView populateInventoryStatuses(ModelAndView modelAndView, Inventory inventory) {
        modelAndView.addObject("inventoryStatusesList", inventoryService.getStatusesList(inventory));
        return modelAndView;
    }

    public ModelAndView populateInventoryChainedLists(ModelAndView modelAndView) throws PortalException, SystemException {
        modelAndView.addObject("statusList", statusService.getList());
        modelAndView.addObject("userList", userService.getList());
        modelAndView.addObject("locationList", organizationService.getLocationList());
        modelAndView.addObject("organizationList", organizationService.getOrganizationList());
        return modelAndView;
    }

    @InitBinder("inventory")
    private void initBinder(WebDataBinder binder) {

        binder.addValidators(inventoryInfoValidator);
        binder.registerCustomEditor(Status.class, new StatusEditor(statusService));

    }

    @RenderMapping(params = "action=inventoryList")
    public ModelAndView inventoryList(@RequestParam(value = "organization_id", required = false) Long organization_id, RenderRequest request, RenderResponse response, Model model) throws Exception {

        searchContainerHelper.putSearchContainerInventory(request, response);
        ModelAndView modelAndView = new ModelAndView(INVENTORY_LIST);

        String condition = "";
        if (organization_id != null) {
            Organization organization = organizationService.getById(organization_id);
            condition = "condition: " + ((OrganizationType.getType(organization) == OrganizationType.ORGANIZATION) ? " organization " : " location ") + " = \"" + organization.getName() + "\"";
        }
        modelAndView.addObject("condition", condition);

        response.addProperty(new Cookie("COOKIE_KEY", "cookie_value"));

        return modelAndView;

    }

    @RenderMapping(params = "action=inventoryAdd")
    public ModelAndView inventoryAdd(RenderRequest request, RenderResponse response, Model model) throws Exception {

        ModelAndView modelAndView;

        // add
        if (!model.containsAttribute("inventory")) {
            modelAndView = new ModelAndView(INVENTORY_UPDATE);
            modelAndView.addObject("inventory", new Inventory());
            modelAndView.addObject("isNotNewInventory", false);
            // edit
        } else {
            modelAndView = new ModelAndView(INVENTORY_UPDATE, (Map<String, ?>) model);
            populateInventoryStatuses(modelAndView, (Inventory) model.asMap().get("inventory"));
            modelAndView.addObject("isNotNewInventory", true);
        }

        return populateInventoryChainedLists(modelAndView);

    }

    @RenderMapping(params = "action=inventoryEdit")
    public ModelAndView inventoryEdit(@RequestParam("id") Integer id, RenderRequest request, RenderResponse response, Model model) throws SystemException, PortalException {

        Inventory inventory = inventoryService.getById(id);
        ModelAndView modelAndView = new ModelAndView(INVENTORY_UPDATE, "inventory", inventory);
        modelAndView.addObject("isNotNewInventory", true);
        populateInventoryStatuses(modelAndView, inventory);
        return populateInventoryChainedLists(modelAndView);

    }

    @RenderMapping(params = "action=inventoryView")
    public ModelAndView statusEdit(@RequestParam("id") Integer id,
                                   RenderRequest request, RenderResponse response, Model model) throws PortalException, SystemException {

        Inventory inventory = inventoryService.getById(id);
        ModelAndView modelAndView = new ModelAndView(INVENTORY_VIEW, "inventoryDTO", inventoryService.getDTOById(id));
        populateInventoryStatuses(modelAndView, inventory);
        return modelAndView;

    }

    @ActionMapping(params = "action=inventorySave")
    public void inventorySave(@Valid Inventory inventory,
                              BindingResult validationResult,
                              ActionRequest request,
                              ActionResponse response,
                              Model model) {

        if (validationResult.hasErrors()) {
            ValidationErrorMaker.makeErrors(model, validationResult.getAllErrors());
            response.setRenderParameter("action", "inventoryAdd");
        } else {
            inventoryService.save(inventory);
            response.setRenderParameter("action", "inventoryList");
        }

    }

    @ActionMapping(params = "action=inventoryDelete")
    public void inventoryDelete(@RequestParam("id") Integer id, ActionResponse response) {
        Inventory inventory = inventoryService.getById(id);
        inventoryService.delete(inventory);
        response.setRenderParameter("action", "inventoryList");
    }

    @ResourceMapping(value = "startURL")
    public View listOrganizationJson(ResourceRequest resourceRequest,
                                     ResourceResponse resourceResponse)
            throws SystemException, PortalException {

        MappingJacksonJsonView view = new MappingJacksonJsonView();

        Set<Status> statuses = statusService.getSet();
        List<OrganizationDTO> dtoList = organizationService.getDTOList();

        Gson gson = new Gson();
        String json = gson.toJson(dtoList);

        view.addStaticAttribute("dtoList", json);
        view.addStaticAttribute("thingObject", statuses);

        return view;

    }

    @ResourceMapping(value = "isNumberValidCheckURL")
    public View isNumberValidCheckURL(@RequestParam("id") Integer id,
                                      @RequestParam("number") String number,
                                      ResourceRequest resourceRequest,
                                      ResourceResponse resourceResponse)
            throws SystemException, PortalException {

        MappingJacksonJsonView view = new MappingJacksonJsonView();
        view.addStaticAttribute("isValid", inventoryService.isNumberNotExist(id, number));

        return view;

    }

    @ResourceMapping(value = "getLocationsByParentOrganizationIdURL")
    public View getLocationsByParentOrganizationIdURL(@RequestParam("id") Long id)
            throws SystemException, PortalException {

        MappingJacksonJsonView view = new MappingJacksonJsonView();

        List<OrganizationDTO> locationDTOList = organizationService.getLocationDTOList(id);

        Gson gson = new Gson();
        String json = gson.toJson(locationDTOList);

        view.addStaticAttribute("dtoList", json);

        return view;

    }

}

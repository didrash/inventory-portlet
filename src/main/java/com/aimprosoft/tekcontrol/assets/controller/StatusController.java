package com.aimprosoft.tekcontrol.assets.controller;

import com.aimprosoft.tekcontrol.assets.domain.Status;
import com.aimprosoft.tekcontrol.assets.service.StatusService;
import com.aimprosoft.tekcontrol.assets.service.ValidationErrorMaker;
import com.aimprosoft.tekcontrol.assets.service.validator.StatusValidator;
import com.aimprosoft.tekcontrol.assets.util.SearchContainerHelper;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.util.PortalUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.json.MappingJacksonJsonView;

import javax.portlet.*;
import javax.servlet.http.Cookie;
import javax.validation.Valid;
import java.util.Map;

import static com.aimprosoft.tekcontrol.assets.util.Constants.*;

/**
 * Created on 6/27/14.
 */
@Controller
@RequestMapping(value = "VIEW")
@SessionAttributes
public class StatusController {

    @Autowired
    protected StatusService statusService;

    @Autowired
    private StatusValidator statusValidator;

    @Autowired
    private SearchContainerHelper searchContainerHelper;

    @InitBinder("status")
    private void initBinder(WebDataBinder binder) {
        binder.addValidators(statusValidator);
    }

    @RenderMapping(params = "action=statusList")
    public ModelAndView statusList(RenderRequest request,
                                   RenderResponse response,
                                   Model model) throws PortalException, SystemException {

        searchContainerHelper.putSearchContainerStatus(request, response);
        ModelAndView modelAndView = new ModelAndView(STATUS_LIST, "statusList", statusService.getList());

        response.addProperty(new Cookie("COOKIE_KEY", "cookie_value"));

        return modelAndView;

    }

    @RenderMapping(params = "action=statusAdd")
    public ModelAndView statusAdd(RenderRequest request, RenderResponse response, Model model) throws Exception {

        ModelAndView modelAndView;

        if (model.asMap().isEmpty()) {
            modelAndView = new ModelAndView(STATUS_UPDATE);
            modelAndView.addObject("status", new Status());
        } else {
            modelAndView = new ModelAndView(STATUS_UPDATE, (Map<String, ?>) model);
        }

        return modelAndView;

    }

    @RenderMapping(params = "action=statusEdit")
    public ModelAndView statusEdit(@RequestParam("id") Integer id,
                                   RenderRequest request, RenderResponse response, Model model) {

        ModelAndView modelAndView = new ModelAndView(STATUS_UPDATE, "status", statusService.getById(id));
        return modelAndView;

    }

    @RenderMapping(params = "action=statusView")
    public ModelAndView statusView(@RequestParam("id") Integer id,
                                   @RequestParam(value = "errorOnDelete", required = false) Boolean errorOnDelete,
                                   RenderRequest request, RenderResponse response, Model model) {

        ModelAndView modelAndView = new ModelAndView(STATUS_VIEW, "status", statusService.getById(id));
        if (errorOnDelete != null && errorOnDelete) {
            modelAndView.addObject("errorOnDelete", "Can't delete status");
        }
        return modelAndView;

    }


    @ActionMapping(params = "action=statusSave")
    public void statusSave(@Valid Status status,
                           BindingResult validationResult,
                           ActionRequest request,
                           ActionResponse response,
                           Model model) {

        if (validationResult.hasErrors()) {
            ValidationErrorMaker.makeErrors(model, validationResult.getAllErrors());
            response.setRenderParameter("action", "statusAdd");
        } else {
            statusService.save(status);
            response.setRenderParameter("action", "statusList");
        }

        Cookie cookie = new Cookie("qwe3", "qwe3");
        cookie.setVersion(0);

        response.addProperty(cookie);
        PortalUtil.getHttpServletResponse(response).addCookie(cookie);

    }

    @ActionMapping(params = "action=statusDelete")
    public void statusDelete(@RequestParam("id") Integer id,
                             ActionResponse response) {

        Status status = statusService.getById(id);
        boolean successDelete = statusService.delete(status);
        if (!successDelete) {
            response.setRenderParameter("id", status.getId().toString());
            response.setRenderParameter("errorOnDelete", "true");
            response.setRenderParameter("action", "statusView");
        } else {
            response.setRenderParameter("action", "statusList");
        }

    }

    @ResourceMapping(value = "isNameValidCheckURL")
    public View isNumberValidCheckURL(@RequestParam("id") Integer id,
                                      @RequestParam("name") String name,
                                      ResourceRequest resourceRequest,
                                      ResourceResponse resourceResponse)
            throws SystemException, PortalException {

        MappingJacksonJsonView view = new MappingJacksonJsonView();
        view.addStaticAttribute("isValid", statusService.isNameNotExist(id, name));
        return view;

    }

}

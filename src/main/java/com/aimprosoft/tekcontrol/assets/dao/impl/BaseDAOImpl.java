package com.aimprosoft.tekcontrol.assets.dao.impl;

import com.aimprosoft.tekcontrol.assets.domain.BaseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

@Repository
public class BaseDAOImpl {

    @PersistenceContext(type = PersistenceContextType.EXTENDED)
    protected EntityManager entityManager;

    @Transactional
    public void create(BaseEntity baseEntity) {
        entityManager.persist(baseEntity);
        entityManager.flush();
    }

    @Transactional
    public void update(BaseEntity baseEntity) {
        entityManager.merge(baseEntity);
        entityManager.flush();
    }

    @Transactional
    public void delete(BaseEntity baseEntity) {
        entityManager.remove(entityManager.contains(baseEntity) ? baseEntity : entityManager.merge(baseEntity));
        entityManager.flush();
    }

}


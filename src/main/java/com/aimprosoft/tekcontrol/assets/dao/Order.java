package com.aimprosoft.tekcontrol.assets.dao;

/**
 * Created on 7/21/14.
 */
public enum Order{

    ASC, DESC;

    @Override
    public String toString() {
        if(this.equals(ASC)){
            return "ASC";
        }else {
            return "DESC";
        }
    }

    public static Order getOrder(String filed){
        if(filed.toUpperCase().contains("ASC")){
            return ASC;
        }else {
            return DESC;
        }
    }

}

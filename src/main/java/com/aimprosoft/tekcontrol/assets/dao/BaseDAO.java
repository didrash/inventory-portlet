package com.aimprosoft.tekcontrol.assets.dao;

import com.aimprosoft.tekcontrol.assets.domain.BaseEntity;

public interface BaseDAO {

    void create(BaseEntity baseEntity);

    void update(BaseEntity baseEntity);

    void delete(BaseEntity baseEntity);

}

package com.aimprosoft.tekcontrol.assets.dao.impl;

import com.aimprosoft.tekcontrol.assets.dao.InventoryDAO;
import com.aimprosoft.tekcontrol.assets.dao.Order;
import com.aimprosoft.tekcontrol.assets.domain.Inventory;
import com.aimprosoft.tekcontrol.assets.util.AssetsGeneralFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional(readOnly = true)
public class InventoryDAOImpl extends BaseDAOImpl implements InventoryDAO {

    private static final Logger _log = LoggerFactory.getLogger(InventoryDAOImpl.class);

    @SuppressWarnings("unchecked")
    @Override
    public List<Inventory> getList() {
        return entityManager.createQuery("from inventories order by id").getResultList();
    }

    @Override
    public int getListCount() {
       Long value = (Long) entityManager.createQuery("select count(inv) from inventories as inv").getSingleResult();
       return value.intValue();
    }

    @Override
    public int getListCountByOrganization(long[] organizationIds) {
        return ((Long) entityManager.
                createQuery("select count(inv) from inventories as inv " +
                        "where (inv.organization_id in (:organizationIds)) " +
                        "or (inv.location_id in (:organizationIds)) ")
                .setParameter("organizationIds", AssetsGeneralFormatter.arrayToListLong(organizationIds))
                .getSingleResult()).intValue();
    }

    @Override
    public List<Inventory> getList(int start, int end, String orderField, Order order) {
        return entityManager
                .createQuery("from inventories order by "+orderField+ " " + order)
                .setFirstResult(start)
                .setMaxResults(end - start)
                .getResultList();
    }

    @Override
    public List<Inventory> getListByOrganization(long[] organizationIds) {

       return entityManager.
                createQuery("from inventories " +
                        "where (organization_id in (:organizationIds)) " +
                        "or (location_id in (:organizationIds)) ")
                .setParameter("organizationIds", AssetsGeneralFormatter.arrayToListLong(organizationIds))
                .getResultList();
    }

    @Override
    public List<Inventory> getListByOrganization(long[] organizationIds, int start, int end, String orderField, Order order) {

        return entityManager.
                createQuery("from inventories " +
                        "where (organization_id in (:organizationIds)) " +
                        "or (location_id in (:organizationIds)) order by "+ orderField + " " + order)
                .setParameter("organizationIds", AssetsGeneralFormatter.arrayToListLong(organizationIds))
                .setFirstResult(start)
                .setMaxResults(end - start)
                .getResultList();

    }

    @Override
    public Inventory getById(int id) {
        return entityManager.find(Inventory.class, id);
    }

    @Override
    @Transactional(readOnly = false)
    public void deleteById(int id) {
        entityManager.createQuery("DELETE from inventories where id = ?1").setParameter(1, id).executeUpdate();
    }

    @Override
    public boolean isNumberNotExist(Integer id, String number) {
        Long count;
        if (id == null) {
            count = (Long) entityManager.createQuery("select count(inv) from inventories as inv where inv.number = ?1")
                    .setParameter(1, number)
                    .getSingleResult();
        } else {
            count = (Long) entityManager.createQuery("select count(inv) from inventories as inv where inv.number = ?1 and id != ?2")
                    .setParameter(1, number)
                    .setParameter(2, id)
                    .getSingleResult();
        }
        return (count == 0 ? true : false);
    }

}

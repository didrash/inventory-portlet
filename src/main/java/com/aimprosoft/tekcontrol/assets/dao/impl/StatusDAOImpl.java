package com.aimprosoft.tekcontrol.assets.dao.impl;

import com.aimprosoft.tekcontrol.assets.dao.Order;
import com.aimprosoft.tekcontrol.assets.dao.StatusDAO;
import com.aimprosoft.tekcontrol.assets.domain.Status;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class StatusDAOImpl extends BaseDAOImpl implements StatusDAO {

    @Override
    public List<Status> getList() {
        return entityManager.createQuery("from status order by id").getResultList();
    }

    @Override
    public List<Status> getList(int start, int end, String orderField, Order order) {
        return entityManager.createQuery("from status order by "+ orderField + " " + order)
                .setFirstResult(start)
                .setMaxResults(end-start)
                .getResultList();
    }

    @Override
    public int getListCount() {
        return ((Long) entityManager.createQuery("select count(st) from status as st").getSingleResult()).intValue();
    }

    @Override
    public Status getById(int id) {
        return entityManager.find(Status.class, id);
    }

    @Override
    public boolean isDeletable(Status status) {

        long value = (Long) entityManager.createQuery("SELECT count(inv) from inventories as inv where inv.status = ?1").setParameter(1, status).getSingleResult();
        if(value != 0){
            return false;
        }
        value = (Long) entityManager.createQuery("SELECT count(inv) from inventorystatuses as inv where inv.status = ?1").setParameter(1, status).getSingleResult();
        if(value != 0){
            return false;
        }
        return true;

    }

    @Override
    public void deleteById(int id) {
        entityManager.createQuery("DELETE from status where id = ?1").setParameter(1, id).executeUpdate();
    }

    @Override
    public boolean isNameNotExist(Integer id, String name) {
        Long count;

        if (id == null) {
            count = (Long) entityManager.createQuery("select count(st) from status as st where st.name = ?1")
                    .setParameter(1, name)
                    .getSingleResult();
        } else {
            count = (Long) entityManager.createQuery("select count(st) from status as st where st.name = ?1 and id != ?2")
                    .setParameter(1, name)
                    .setParameter(2, id)
                    .getSingleResult();
        }

        return (count == 0 ? true : false);

    }

}

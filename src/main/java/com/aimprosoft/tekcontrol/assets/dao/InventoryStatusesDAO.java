package com.aimprosoft.tekcontrol.assets.dao;

import com.aimprosoft.tekcontrol.assets.domain.Inventory;
import com.aimprosoft.tekcontrol.assets.domain.InventoryStatuses;
import com.aimprosoft.tekcontrol.assets.domain.Status;

import java.util.Date;
import java.util.List;
import java.util.Set;

public interface InventoryStatusesDAO {

    List<InventoryStatuses> getListByInventory(Inventory inventory);

    Set<InventoryStatuses> getSetByInventory(Inventory inventory);

    Set<InventoryStatuses> getSetByInventoryId(int id);

    void create(InventoryStatuses inventoryStatuses);

    void deleteByInventory(Inventory inventory);

    boolean isNotLast (Inventory inventory, Status status);

    Date getLastDate(Inventory inventory);

}

package com.aimprosoft.tekcontrol.assets.dao;

import com.aimprosoft.tekcontrol.assets.domain.Status;

import java.util.List;

/**
 * Created on 6/27/14.
 */
public interface StatusDAO extends BaseDAO{

    List<Status> getList();

    List<Status> getList(int start, int end, String orderField, Order order);

    int getListCount();

    Status getById(int id);

    boolean isDeletable(Status status);

    void deleteById(int id);

    boolean isNameNotExist(Integer id, String name);

}

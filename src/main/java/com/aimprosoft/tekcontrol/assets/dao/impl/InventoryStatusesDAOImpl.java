package com.aimprosoft.tekcontrol.assets.dao.impl;

import com.aimprosoft.tekcontrol.assets.dao.InventoryStatusesDAO;
import com.aimprosoft.tekcontrol.assets.domain.Inventory;
import com.aimprosoft.tekcontrol.assets.domain.InventoryStatuses;
import com.aimprosoft.tekcontrol.assets.domain.Status;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

@Repository
@Transactional(readOnly = true)
public class InventoryStatusesDAOImpl extends BaseDAOImpl implements InventoryStatusesDAO {

    @Override
    public List<InventoryStatuses> getListByInventory(Inventory inventory) {
        return entityManager.createQuery("from inventorystatuses where inventory_id = ?1 order by date").
                setParameter(1, inventory.getId()).
                getResultList();
    }

    @Override
    public Set<InventoryStatuses> getSetByInventory(Inventory inventory) {
        return getSetByInventoryId(inventory.getId());
    }

    @Override
    public Set<InventoryStatuses> getSetByInventoryId(int id) {
        return new LinkedHashSet<InventoryStatuses>(entityManager.createQuery("from inventorystatuses where inventory_id = ?1 order by date").setParameter(1, id).getResultList());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
    public void create(InventoryStatuses inventoryStatuses) {
        entityManager.persist(inventoryStatuses);
    }

    @Override
    public void deleteByInventory(Inventory inventory) {
        Set<InventoryStatuses> inventoryStatuses = getSetByInventory(inventory);
        for (InventoryStatuses inventoryStatus : inventoryStatuses) {
            entityManager.remove(inventoryStatus);
        }
    }

    @Override
    public boolean isNotLast(Inventory inventory, Status status) {

        if (inventory.isNew()) {
            return true;
        }

        List<InventoryStatuses> inventoryStatusesList =
                entityManager.createQuery("from inventorystatuses where inventory_id = ?1 order by date DESC").setParameter(1, inventory.getId())
                        .setMaxResults(1)
                        .getResultList();

        if (!inventoryStatusesList.isEmpty() && (inventoryStatusesList.get(0).getStatus() == status)) {
            return false;
        } else {
            return true;
        }

    }

    @Override
    public Date getLastDate(Inventory inventory) {

        Date date;
        if (inventory.isNew()) {
            date = new Date();
        } else {

            date = (Date) entityManager.createQuery("select inv.date from inventorystatuses as inv where inv.inventory = ?1 order by inv.date DESC")
                    .setParameter(1, inventory)
                    .setMaxResults(1)
                    .getSingleResult();
        }
        return date;
    }

}

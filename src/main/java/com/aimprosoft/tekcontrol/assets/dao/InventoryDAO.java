package com.aimprosoft.tekcontrol.assets.dao;

import com.aimprosoft.tekcontrol.assets.domain.Inventory;

import java.util.List;

/**
 * Created on 6/27/14.
 */
public interface InventoryDAO extends BaseDAO{

    List<Inventory> getList();

    int getListCount();

    int getListCountByOrganization(long[] organizationIds);

    List<Inventory> getList(int start, int end, String orderField, Order order);

    List<Inventory> getListByOrganization(long[] organizationIds);

    List<Inventory> getListByOrganization(long[] organizationIds, int start, int end, String orderField, Order order);

    Inventory getById(int id);

    void deleteById(int id);

    boolean isNumberNotExist(Integer id, String number);

}

package com.aimprosoft.tekcontrol.assets.view.searchContainer.inventory;


import com.liferay.portal.kernel.dao.search.DAOParamUtil;

import javax.portlet.PortletRequest;

/**
 * Created on 7/18/14.
 */
public class InventorySearchTerms extends InventoryDisplayTerms {

    public InventorySearchTerms(PortletRequest portletRequest) {
        super(portletRequest);

        id = DAOParamUtil.getInteger(portletRequest, ID);
        name = DAOParamUtil.getString(portletRequest, NAME);
        number = DAOParamUtil.getString(portletRequest, NUMBER);
        status = DAOParamUtil.getString(portletRequest, STATUS);
        user =  DAOParamUtil.getString(portletRequest, USER);
        organization = DAOParamUtil.getString(portletRequest, ORGANIZATION);
        location = DAOParamUtil.getString(portletRequest, LOCATION);

    }

}

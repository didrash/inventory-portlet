package com.aimprosoft.tekcontrol.assets.view.searchContainer.inventory;

import com.liferay.portal.kernel.dao.search.DisplayTerms;
import com.liferay.portal.kernel.util.ParamUtil;

import javax.portlet.PortletRequest;
import java.util.Date;

/**
 * Created on 7/18/14.
 */
public class InventoryDisplayTerms extends DisplayTerms {

    public static final String ID = "id";

    public static final String NAME = "name";

    public static final String NUMBER = "number";

    public static final String USER = "user";

    public static final String ORGANIZATION = "middleName";

    public static final String LOCATION = "organizationId";

    public static final String STATUS = "status";

    public static final String DATEOFSTATUS = "dateOfStatus";

    protected Integer id;
    protected String name;
    protected String number;
    protected String status;
    protected String user;
    protected String organization;
    protected String location;
    protected Date dateOfStatus;

    public InventoryDisplayTerms(PortletRequest portletRequest) {
        super(portletRequest);

        String statusString = ParamUtil.getString(portletRequest, STATUS);

        id = ParamUtil.getInteger(portletRequest, ID);
        name = ParamUtil.getString(portletRequest, NAME);
        number = ParamUtil.getString(portletRequest, NUMBER);
        status = ParamUtil.getString(portletRequest, STATUS);
        user =  ParamUtil.getString(portletRequest, USER);
        organization = ParamUtil.getString(portletRequest, ORGANIZATION);
        location = ParamUtil.getString(portletRequest, LOCATION);

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Date getDateOfStatus() {
        return dateOfStatus;
    }

    public void setDateOfStatus(Date dateOfStatus) {
        this.dateOfStatus = dateOfStatus;
    }

}

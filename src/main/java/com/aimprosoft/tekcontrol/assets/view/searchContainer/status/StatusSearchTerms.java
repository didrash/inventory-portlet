package com.aimprosoft.tekcontrol.assets.view.searchContainer.status;


import com.aimprosoft.tekcontrol.assets.view.searchContainer.inventory.InventoryDisplayTerms;
import com.liferay.portal.kernel.dao.search.DAOParamUtil;

import javax.portlet.PortletRequest;

/**
 * Created on 7/18/14.
 */
public class StatusSearchTerms extends InventoryDisplayTerms {

    public StatusSearchTerms(PortletRequest portletRequest) {
        super(portletRequest);

        id = DAOParamUtil.getInteger(portletRequest, ID);
        name = DAOParamUtil.getString(portletRequest, NAME);

    }

}

package com.aimprosoft.tekcontrol.assets.view.searchContainer.status;

import com.aimprosoft.tekcontrol.assets.domain.Status;
import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.util.PortletKeys;
import com.liferay.portlet.PortalPreferences;
import com.liferay.portlet.PortletPreferencesFactoryUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.portlet.PortletConfig;
import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StatusSearchContainer extends SearchContainer<Status> {

    private static final Logger _log = LoggerFactory.getLogger(StatusSearchContainer.class);

    static List<String> headerNames = new ArrayList<String>();
    static Map<String, String> orderableHeaders = new HashMap<String, String>();

    static {

        headerNames.add("Name");
        orderableHeaders.put("Name", "Name");

    }

    public static final String EMPTY_RESULTS_MESSAGE = "No statuses were found";

    public StatusSearchContainer(PortletRequest portletRequest, PortletURL iteratorURL, Integer delta, Integer page) {
        this(portletRequest, DEFAULT_CUR_PARAM, iteratorURL, delta, page);
    }

    public StatusSearchContainer(
            PortletRequest portletRequest, String curParam,
            PortletURL iteratorURL,
            Integer delta, Integer page) {

        super(portletRequest, new StatusDisplayTerms(portletRequest),
                new StatusSearchTerms(portletRequest), curParam, page,
                //ParamUtil.getInteger(portletRequest, SearchContainer.DEFAULT_DELTA_PARAM, 5),
                delta,
                iteratorURL, headerNames, EMPTY_RESULTS_MESSAGE);

        String portletId = (String) portletRequest.getAttribute(WebKeys.PORTLET_ID);

        PortletConfig portletConfig = (PortletConfig) portletRequest.getAttribute(JavaConstants.JAVAX_PORTLET_CONFIG);
        StatusDisplayTerms displayTerms = (StatusDisplayTerms) getDisplayTerms();
        StatusSearchTerms searchTerms = (StatusSearchTerms) getSearchTerms();

        String portletName = portletConfig.getPortletName();

        iteratorURL.setParameter(StatusDisplayTerms.NAME, displayTerms.getName());

        try {

            PortalPreferences preferences = PortletPreferencesFactoryUtil.getPortalPreferences(portletRequest);

            String orderByCol = ParamUtil.getString(portletRequest, "orderByCol");
            String orderByType = ParamUtil.getString(portletRequest, "orderByType");

            if (Validator.isNotNull(orderByCol) &&
                    Validator.isNotNull(orderByType)) {

                preferences.setValue(PortletKeys.USERS_ADMIN, "status-order-by-col", orderByCol);
                preferences.setValue(PortletKeys.USERS_ADMIN, "status-order-by-type", orderByType);

            } else {

                orderByCol = preferences.getValue(PortletKeys.USERS_ADMIN, "status-order-by-col", "name");
                orderByType = preferences.getValue(PortletKeys.USERS_ADMIN, "status-order-by-type", "asc");

            }

            setOrderableHeaders(orderableHeaders);
            setOrderByCol(orderByCol);
            setOrderByType(orderByType);

        } catch (Exception e) {
            _log.error("Occurred in StatusSearchContainer", e);
        }
    }

}

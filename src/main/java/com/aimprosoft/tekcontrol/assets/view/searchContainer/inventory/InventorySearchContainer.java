package com.aimprosoft.tekcontrol.assets.view.searchContainer.inventory;

import com.aimprosoft.tekcontrol.assets.dto.InventoryDTO;
import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.util.PortletKeys;
import com.liferay.portlet.PortalPreferences;
import com.liferay.portlet.PortletPreferencesFactoryUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created on 7/18/14.
 */
public class InventorySearchContainer extends SearchContainer<InventoryDTO> {

    private static final Logger _log = LoggerFactory.getLogger(InventorySearchContainer.class);

    private static List<String> headerNames = new ArrayList<String>();
    static Map<String, String> orderableHeaders = new HashMap<String, String>();

    static {

        headerNames.add("Name");
        headerNames.add("Number");
        headerNames.add("Assigned user");
        headerNames.add("Organization");
        headerNames.add("Location");
        headerNames.add("Status");
        headerNames.add("Date of status");

        orderableHeaders.put("Name", "Name");
        orderableHeaders.put("Number", "Number");
        orderableHeaders.put("Organization", "Organization");
        orderableHeaders.put("Location", "Location");
        orderableHeaders.put("Status", "Status");

    }

    public static final String EMPTY_RESULTS_MESSAGE = "No inventories were found";

    public InventorySearchContainer(PortletRequest portletRequest, PortletURL iteratorURL, Integer delta, Integer page) {
        this(portletRequest, DEFAULT_CUR_PARAM, iteratorURL, delta, page);
    }

    public InventorySearchContainer(
            PortletRequest portletRequest, String curParam,
            PortletURL iteratorURL, Integer delta, Integer page) {

        super(portletRequest, new InventoryDisplayTerms(portletRequest),
                new InventorySearchTerms(portletRequest), curParam, page,
                //ParamUtil.getInteger(portletRequest, SearchContainer.DEFAULT_DELTA_PARAM, 5),
                delta,
                iteratorURL, headerNames, EMPTY_RESULTS_MESSAGE);


        InventoryDisplayTerms displayTerms = (InventoryDisplayTerms)getDisplayTerms();

        iteratorURL.setParameter(InventoryDisplayTerms.NAME, displayTerms.getName());
        iteratorURL.setParameter(InventoryDisplayTerms.NUMBER, displayTerms.getNumber());
        iteratorURL.setParameter(InventoryDisplayTerms.ORGANIZATION, displayTerms.getOrganization());
        iteratorURL.setParameter(InventoryDisplayTerms.LOCATION, displayTerms.getLocation());
        iteratorURL.setParameter(InventoryDisplayTerms.STATUS, displayTerms.getStatus());
        iteratorURL.setParameter(InventoryDisplayTerms.USER, displayTerms.getUser());

        try {

            PortalPreferences preferences = PortletPreferencesFactoryUtil.getPortalPreferences(portletRequest);

            String orderByCol = ParamUtil.getString(portletRequest, "orderByCol");
            String orderByType = ParamUtil.getString(portletRequest, "orderByType");

            if (Validator.isNotNull(orderByCol) &&
                    Validator.isNotNull(orderByType)) {

                preferences.setValue(PortletKeys.USERS_ADMIN, "inventory-order-by-col", orderByCol);
                preferences.setValue(PortletKeys.USERS_ADMIN, "inventory-order-by-type", orderByType);

            }
            else {

                orderByCol = preferences.getValue(PortletKeys.USERS_ADMIN, "inventory-order-by-col", "name");
                orderByType = preferences.getValue(PortletKeys.USERS_ADMIN, "inventory-order-by-type", "asc");

            }

            setOrderableHeaders(orderableHeaders);
            setOrderByCol(orderByCol);
            setOrderByType(orderByType);

        }
        catch (Exception e) {
            _log.error("Occurred in InventorySearchContainer", e);
        }
    }

}

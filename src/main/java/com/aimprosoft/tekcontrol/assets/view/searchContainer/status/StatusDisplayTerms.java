package com.aimprosoft.tekcontrol.assets.view.searchContainer.status;

import com.liferay.portal.kernel.dao.search.DisplayTerms;
import com.liferay.portal.kernel.util.ParamUtil;

import javax.portlet.PortletRequest;

/**
 * Created on 7/18/14.
 */
public class StatusDisplayTerms extends DisplayTerms {

    public static final String ID = "id";

    public static final String NAME = "name";

    protected Integer id;
    protected String name;

    public StatusDisplayTerms(PortletRequest portletRequest) {
        super(portletRequest);

        id = ParamUtil.getInteger(portletRequest, ID);
        name = ParamUtil.getString(portletRequest, NAME);

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

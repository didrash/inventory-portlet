package com.aimprosoft.tekcontrol.assets.util;

/**
 * Created by user on 3/12/14.
 */
public class Constants {

    // Index
    public static final String INDEX_VIEW = "index";
    public static final String INDEX_MESSAGE = "message";
    public static final String ERROR_MESSAGE = "error";


    // Inventory
    public static final String INVENTORY_LIST = "inventory/list";
    public static final String INVENTORY_VIEW = "inventory/view";
    public static final String INVENTORY_UPDATE = "inventory/edit";

    // Status
    public static final String STATUS_LIST = "status/list";
    public static final String STATUS_VIEW = "status/view";
    public static final String STATUS_UPDATE = "status/edit";

    // messages
    public static final String MESSAGE_REQUIRED_FIELD = "Required field";


}


package com.aimprosoft.tekcontrol.assets.util;

import com.aimprosoft.tekcontrol.assets.domain.Status;
import com.aimprosoft.tekcontrol.assets.dto.InventoryDTO;
import com.aimprosoft.tekcontrol.assets.service.InventoryService;
import com.aimprosoft.tekcontrol.assets.service.StatusService;
import com.aimprosoft.tekcontrol.assets.view.searchContainer.inventory.InventorySearchContainer;
import com.aimprosoft.tekcontrol.assets.view.searchContainer.status.StatusSearchContainer;
import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.PortletPreferencesFactoryUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.portlet.PortletPreferences;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;

@Component
public class SearchContainerHelper {

    private static final Logger _log = LoggerFactory.getLogger(SearchContainerHelper.class);

    @Autowired
    private StatusService statusService;

    @Autowired
    private InventoryService inventoryService;

    final Integer DEFAULT_DELTA = 5;

    public void putSearchContainerStatus(RenderRequest renderRequest, RenderResponse renderResponse) throws SystemException, PortalException {

        final String DELTA_NAME = "delta_status";

        PortletURL viewList = renderResponse.createRenderURL();
        viewList.setParameter("action", "statusList");

        Integer delta = calculateDelta(renderRequest, DELTA_NAME);
        Integer page = calculatePage(renderRequest, DELTA_NAME);

        SearchContainer<Status> searchContainer = new StatusSearchContainer(renderRequest, SearchContainer.DEFAULT_CUR_PARAM, viewList, delta, page);

        List<Status> list = statusService.getList(searchContainer.getStart(),
                searchContainer.getEnd(),
                searchContainer.getOrderByCol(),
                searchContainer.getOrderByType());

        int total = statusService.getListCount();

        if((total != 0) && (list.isEmpty())){
            setFirstPage(renderRequest, DELTA_NAME);
        }

        searchContainer.setTotal(total);
        searchContainer.setResults(list);
        renderRequest.setAttribute("searchContainer", searchContainer);

    }

    public void putSearchContainerInventory(RenderRequest renderRequest, RenderResponse renderResponse) throws PortalException, SystemException {

        final String DELTA_NAME = "delta_inventory";

        PortletURL viewList = renderResponse.createRenderURL();
        viewList.setParameter("action", "inventoryList");

        List<InventoryDTO> list = Collections.emptyList();
        int total = 0;

        Integer delta = calculateDelta(renderRequest, DELTA_NAME);
        Integer page = calculatePage(renderRequest, DELTA_NAME);

        SearchContainer<InventoryDTO> searchContainer = new InventorySearchContainer(renderRequest, viewList, delta, page);


        try {
            String organization_id = renderRequest.getParameter("organization_id");
            if ((organization_id != null) && (!organization_id.isEmpty())) {
                viewList.setParameter("organization_id", organization_id);

                Long organizationIdLong = Long.parseLong(organization_id);

                list = inventoryService.getDtoListByOrganization(
                        organizationIdLong,
                        searchContainer.getStart(),
                        searchContainer.getEnd(),
                        searchContainer.getOrderByCol(),
                        searchContainer.getOrderByType());

                total = inventoryService.getListCountByOrganization(organizationIdLong);


            } else {

                list = inventoryService.getDtoList(
                        searchContainer.getStart(),
                        searchContainer.getEnd(),
                        searchContainer.getOrderByCol(),
                        searchContainer.getOrderByType());

                total = inventoryService.getListCount();

            }
        } catch (Exception e) {
            _log.error("from search container helper in inventory option", e);
        }

        if((total != 0) && (list.isEmpty())){
            setFirstPage(renderRequest, DELTA_NAME);
        }

        searchContainer.setTotal(total);
        searchContainer.setResults(list);
        renderRequest.setAttribute("searchContainer", searchContainer);

    }

    private Integer calculateDelta(RenderRequest renderRequest, String deltaName) throws SystemException, PortalException {

        boolean saveDelta = false;

        User user = (User) renderRequest.getAttribute(WebKeys.USER);

        String fullDeltaName = deltaName + user.getUserId();

        HttpServletRequest request = PortalUtil.getHttpServletRequest(renderRequest);
        String portletId = AssetsGeneralUtil.getPortletID(renderRequest);

        PortletPreferences portletPreferences = PortletPreferencesFactoryUtil.getPortletPreferences(request, portletId);

        String value = portletPreferences.getValue(fullDeltaName, StringPool.BLANK);
        Integer deltaFromPreferences = -1;
        if ((value != null) && (!value.isEmpty())) {
            deltaFromPreferences = Integer.parseInt(value);
        }

        Integer delta = 0;
        Integer deltaFromRequest = ParamUtil.getInteger(renderRequest, "delta");

        if (deltaFromRequest != 0) {
            delta = deltaFromRequest;
            saveDelta = true;
        } else if (deltaFromPreferences != -1) {
            delta = deltaFromPreferences;
        } else {
            delta = DEFAULT_DELTA;
        }

        if (saveDelta) {
            AssetsGeneralUtil.savePortletValue(portletPreferences, fullDeltaName, String.valueOf(delta));
        }

        return delta;

    }

    private Integer calculatePage(RenderRequest renderRequest, String pageName) {

        boolean changeValue = false;
        Integer page = 1;
        Integer currentPage = ParamUtil.getInteger(renderRequest, SearchContainer.DEFAULT_CUR_PARAM);
        if (currentPage != 0) {
            page = currentPage;
            changeValue = true;
        } else if (currentPage == 0) {
            Integer sessionValue = (Integer) renderRequest.getPortletSession(false).getAttribute(pageName);
            if (sessionValue != null) {
                page = sessionValue;
            }
        }

        if (changeValue){
            renderRequest.getPortletSession(false).setAttribute(pageName, page);
        }

        return page;

    }

    private void setFirstPage(RenderRequest renderRequest, String pageName){
        renderRequest.getPortletSession(false).setAttribute(pageName, 1);
    }

}

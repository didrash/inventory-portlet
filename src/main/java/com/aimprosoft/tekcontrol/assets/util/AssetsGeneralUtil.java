package com.aimprosoft.tekcontrol.assets.util;

import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.WebKeys;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.portlet.PortletPreferences;
import javax.portlet.RenderRequest;

/**
 * Created on 8/1/14.
 */
public class AssetsGeneralUtil {

    private static final Logger _log = LoggerFactory.getLogger(AssetsGeneralUtil.class);

    public static String getPortletID(RenderRequest renderRequest){
        return  (String) renderRequest.getAttribute(WebKeys.PORTLET_ID);
    }

    public static boolean savePortletValue(PortletPreferences portletPreferences, String key, String value){

        boolean success = true;

        try {
            portletPreferences.setValue(key, value);
            portletPreferences.store();
        } catch (Exception e) {
            _log.error("from savePortletValue", e);
            success = false;
        }
        return success;

    }

    public static String getPortletValue(PortletPreferences portletPreferences, String key){

        return portletPreferences.getValue(key, StringPool.BLANK);

    }


}

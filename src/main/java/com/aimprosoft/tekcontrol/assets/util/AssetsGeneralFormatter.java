package com.aimprosoft.tekcontrol.assets.util;

import java.util.ArrayList;
import java.util.List;

public class AssetsGeneralFormatter {

    protected AssetsGeneralFormatter() {
    }

    public static List<Long> arrayToListLong(long[] array){

        List<Long> list = new ArrayList<Long>(array.length);

        for(int i = 0; i < array.length; i++){
            list.add(array[i]);
        }

        return list;

    }



}

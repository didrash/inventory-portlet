package com.aimprosoft.tekcontrol.assets.dto;

import com.aimprosoft.tekcontrol.assets.domain.Inventory;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.service.OrganizationLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;

public class InventoryDTO {

    private Integer id;
    private String name;
    private String number;
    private String status;
    private String user = "";
    private String organization = "";
    private String location = "";
    private String notes = "";
    private String dateOfStatus;

    public InventoryDTO() {

    }

    public InventoryDTO(Inventory inventory) throws SystemException, PortalException {
        
        this.id = inventory.getId();
        this.name = inventory.getName();
        this.number = inventory.getNumber();

        if (inventory.getStatus() != null) {
            this.status = inventory.getStatus().getName();
        }
        if (inventory.getUser_id() != null) {
            this.user = UserLocalServiceUtil.getUser(inventory.getUser_id()).getFullName();
        }
        if (inventory.getOrganization_id() != null) {
            this.organization = OrganizationLocalServiceUtil.getOrganization(inventory.getOrganization_id()).getName();
        }
        if (inventory.getLocation_id() != null) {
            this.location = OrganizationLocalServiceUtil.getOrganization(inventory.getLocation_id()).getName();
        }
        if (!inventory.getNotes().isEmpty()) {
            this.notes = inventory.getNotes();
        }

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getDateOfStatus() {
        return dateOfStatus;
    }

    public void setDateOfStatus(String dateOfStatus) {
        this.dateOfStatus = dateOfStatus;
    }

}

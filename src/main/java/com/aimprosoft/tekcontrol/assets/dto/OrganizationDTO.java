package com.aimprosoft.tekcontrol.assets.dto;

import com.aimprosoft.tekcontrol.assets.service.OrganizationType;
import com.liferay.portal.model.Organization;
import org.codehaus.jackson.annotate.JsonProperty;


public class OrganizationDTO {

    @JsonProperty("id")
    private String id;
    @JsonProperty("parentid")
    private String parentid;
    @JsonProperty("name")
    private String name;
    @JsonProperty("type")
    private OrganizationType type;

    public OrganizationDTO(Organization organization) {
        this.id = String.valueOf(organization.getOrganizationId());
        this.parentid = String.valueOf(organization.getParentOrganizationId());
        this.name = organization.getName();
        this.type = OrganizationType.getType(organization);
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public OrganizationType getType() {
        return type;
    }

    public void setType(OrganizationType type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParentid() {
        return parentid;
    }

    public void setParentid(String parentid) {
        this.parentid = parentid;
    }
}


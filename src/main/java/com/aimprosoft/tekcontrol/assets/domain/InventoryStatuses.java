package com.aimprosoft.tekcontrol.assets.domain;

import org.hibernate.annotations.ForeignKey;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "inventorystatuses")
public class InventoryStatuses extends BaseEntity {

    public InventoryStatuses() {
    }

    public InventoryStatuses(Inventory inventory, Status status, Date date) {
        this.inventory = inventory;
        this.status = status;
        this.date = date;
    }

    @ManyToOne(targetEntity = Inventory.class)
    @ForeignKey(name = "FKInventoryId")
    private Inventory inventory;

    @ManyToOne(targetEntity = Status.class)
    @ForeignKey(name = "FKStatusId")
    private Status status;

    private Date date;

    public Inventory getInventory() {
        return inventory;
    }

    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}

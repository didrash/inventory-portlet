package com.aimprosoft.tekcontrol.assets.domain;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

import static com.aimprosoft.tekcontrol.assets.util.Constants.MESSAGE_REQUIRED_FIELD;

/**
 * Created on 6/27/14.
 */
@Entity(name = "status")
public class Status extends BaseEntity {

    @NotNull(message = MESSAGE_REQUIRED_FIELD)
    @NotEmpty(message = MESSAGE_REQUIRED_FIELD)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

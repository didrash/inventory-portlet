package com.aimprosoft.tekcontrol.assets.domain;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

import static com.aimprosoft.tekcontrol.assets.util.Constants.MESSAGE_REQUIRED_FIELD;

@Entity(name = "inventories")
public class Inventory extends BaseEntity {

    @NotNull(message = MESSAGE_REQUIRED_FIELD)
    @NotEmpty(message = MESSAGE_REQUIRED_FIELD)
    private String name;

    @NotNull(message = MESSAGE_REQUIRED_FIELD)
    @NotEmpty(message = MESSAGE_REQUIRED_FIELD)
    private String number;

    @OneToMany(targetEntity=InventoryStatuses.class, mappedBy="inventory", cascade={CascadeType.ALL})
    private Set<InventoryStatuses> inventoryStatuses;

    @ManyToOne(targetEntity = Status.class, fetch = FetchType.LAZY)
    @NotNull(message = MESSAGE_REQUIRED_FIELD)
    @ForeignKey(name = "FKStatusId")
    private Status status;

    private Long user_id;
    private Long organization_id;
    private Long location_id;

    private String notes;

    public Inventory() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

     public Set<InventoryStatuses> getInventoryStatuses() {
        return inventoryStatuses;
    }

    public void setInventoryStatuses(Set<InventoryStatuses> inventoryStatuses) {
        this.inventoryStatuses = inventoryStatuses;
    }


    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public Long getLocation_id() {
        return location_id;
    }

    public void setLocation_id(Long location_id) {
        this.location_id = location_id;
    }

    public Long getOrganization_id() {
        return organization_id;
    }

    public void setOrganization_id(Long organization_id) {
        this.organization_id = organization_id;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

}


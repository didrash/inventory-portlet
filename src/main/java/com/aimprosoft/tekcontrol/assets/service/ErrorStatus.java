package com.aimprosoft.tekcontrol.assets.service;

/**
 * Created on 4/16/14.
 */
public class ErrorStatus {

    private boolean error;
    private String errorMessage;

    public ErrorStatus(){

    }

    public ErrorStatus(boolean error, String errorMessage){
        this.error = error;
        this.errorMessage = errorMessage;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}

package com.aimprosoft.tekcontrol.assets.service.validator;

import com.aimprosoft.tekcontrol.assets.domain.Status;
import com.aimprosoft.tekcontrol.assets.service.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class StatusValidator implements Validator{

    @Autowired
    private StatusService statusService;

    @Override
    public boolean supports(Class<?> clazz) {
        return Status.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Status status = (Status) target;
        if (!statusService.isNameNotExist(status)){
            errors.rejectValue("name", "this name has already existed", "this name has already existed");
        }
    }
}

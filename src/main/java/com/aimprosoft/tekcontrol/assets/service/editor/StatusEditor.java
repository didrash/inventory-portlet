package com.aimprosoft.tekcontrol.assets.service.editor;

import com.aimprosoft.tekcontrol.assets.service.StatusService;

import java.beans.PropertyEditorSupport;

public class StatusEditor extends PropertyEditorSupport {

    private StatusService statusService;

    public StatusEditor(StatusService statusService) {
        this.statusService = statusService;
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        if(!text.equals("")) {
            setValue(statusService.getById(Integer.parseInt(text)));
        }else {
            setValue(null);
        }
    }

}

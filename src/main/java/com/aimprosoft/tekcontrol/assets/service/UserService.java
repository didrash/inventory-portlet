package com.aimprosoft.tekcontrol.assets.service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.User;

import java.util.List;

public interface UserService {

    List<User> getList() throws SystemException, PortalException;

}

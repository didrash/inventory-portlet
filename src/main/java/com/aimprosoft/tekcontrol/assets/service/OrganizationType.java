package com.aimprosoft.tekcontrol.assets.service;

import com.liferay.portal.model.Organization;

/**
 * Created on 7/2/14.
 */
public enum OrganizationType {

    ORGANIZATION, LOCATION;

    public static OrganizationType getType(Organization organization) {

        String type = organization.getType();

        if (type.equals("regular-organization"))
            return OrganizationType.ORGANIZATION;
        else if (type.equals("location")) {
            return OrganizationType.LOCATION;
        } else {
            return null;
        }

    }

    @Override
    public String toString(){

        if(this == OrganizationType.ORGANIZATION) {
            return "regular-organization";
        }
        else if (this == OrganizationType.LOCATION){
            return "location";
        }else{
            return super.toString();
        }

    }

}

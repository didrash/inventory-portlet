package com.aimprosoft.tekcontrol.assets.service;

import com.aimprosoft.tekcontrol.assets.dao.Order;
import com.aimprosoft.tekcontrol.assets.domain.Inventory;
import com.aimprosoft.tekcontrol.assets.domain.InventoryStatuses;
import com.aimprosoft.tekcontrol.assets.dto.InventoryDTO;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

import java.util.Date;
import java.util.List;

public interface InventoryService{

    List<Inventory> getList();

    int getListCount();

    int getListCountByOrganization(long organizationId);

    List<Inventory> getListByOrganization(long organizationId);

    List<InventoryDTO> getDtoList() throws Exception;

    List<InventoryDTO> getDtoListByOrganization(long organizationId) throws Exception;

    List<InventoryDTO> getDtoList(int start, int end, String orderField, Order order) throws Exception;

    List<InventoryDTO> getDtoList(int start, int end, String orderField, String order) throws Exception;

    List<InventoryDTO> getDtoListByOrganization(long organizationId, int start, int end, String orderField, Order order)
            throws Exception;

    List<InventoryDTO> getDtoListByOrganization(long organizationId, int start, int end, String orderField, String order)
            throws Exception;

    Inventory getById(int id);

    InventoryDTO getDTOById(int id) throws SystemException, PortalException;

    void save(Inventory inventory);

    void delete(Inventory inventory);

    boolean isNumberNotExist(Inventory inventory);

    boolean isNumberNotExist(Integer id, String number);

    List<InventoryStatuses> getStatusesList(Inventory inventory);

    Date getLastStatusDate(Inventory inventory);

}

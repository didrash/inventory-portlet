package com.aimprosoft.tekcontrol.assets.service.impl;

import com.aimprosoft.tekcontrol.assets.dao.InventoryDAO;
import com.aimprosoft.tekcontrol.assets.dao.InventoryStatusesDAO;
import com.aimprosoft.tekcontrol.assets.dao.Order;
import com.aimprosoft.tekcontrol.assets.domain.Inventory;
import com.aimprosoft.tekcontrol.assets.domain.InventoryStatuses;
import com.aimprosoft.tekcontrol.assets.dto.InventoryDTO;
import com.aimprosoft.tekcontrol.assets.service.InventoryService;
import com.aimprosoft.tekcontrol.assets.service.OrganizationService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class InventoryServiceImpl implements InventoryService{

    @Autowired
    private InventoryDAO inventoryDAO;

    @Autowired
    private InventoryStatusesDAO inventoryStatusesDAO;

    @Autowired
    private OrganizationService organizationService;

    @Override
    @Transactional(readOnly = true)
    public List<Inventory> getList() {
        return inventoryDAO.getList();
    }

    @Override
    @Transactional(readOnly = true)
    public int getListCount() {
        return inventoryDAO.getListCount();
    }

    @Override
    @Transactional(readOnly = true)
    public int getListCountByOrganization(long organizationId) {
        return inventoryDAO.getListCountByOrganization(organizationService.getNestedIds(organizationId));
    }

    @Override
    @Transactional(readOnly = true)
    public List<Inventory> getListByOrganization(long organizationId) {
        return inventoryDAO.getListByOrganization(organizationService.getNestedIds(organizationId));
    }

    @Override
    @Transactional(readOnly = true)
    public List<InventoryDTO> getDtoList() throws Exception {
        return createInventoryDTOList(getList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<InventoryDTO> getDtoListByOrganization(long organizationId) throws Exception {
        return createInventoryDTOList(getListByOrganization(organizationId));
    }

    @Override
    @Transactional(readOnly = true)
    public List<InventoryDTO> getDtoList(int start, int end, String orderField, Order order) throws Exception {
        return createInventoryDTOList(inventoryDAO.getList(start, end, orderField, order));
    }

    @Override
    public List<InventoryDTO> getDtoList(int start, int end, String orderField, String order) throws Exception {
        return getDtoList(start, end, orderField, Order.getOrder(order));
    }

    @Override
    @Transactional(readOnly = true)
    public List<InventoryDTO> getDtoListByOrganization(long organizationId, int start, int end, String orderField, Order order) throws Exception {
        return createInventoryDTOList(inventoryDAO.getListByOrganization(organizationService.getNestedIds(organizationId), start, end, orderField, order));
    }

    @Override
    public List<InventoryDTO> getDtoListByOrganization(long organizationId, int start, int end, String orderField, String order) throws Exception {
        return getDtoListByOrganization(organizationId, start, end, orderField, Order.getOrder(order));
    }

    @Override
    @Transactional(readOnly = true)
    public Inventory getById(int id) {
        return inventoryDAO.getById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public InventoryDTO getDTOById(int id) throws SystemException, PortalException {
        return createInventoryDTO(inventoryDAO.getById(id));
    }

    @Override
    public void save(Inventory inventory) {

        boolean isNew = inventory.isNew();
        boolean needToSave = isNew || (inventoryStatusesDAO.isNotLast(inventory, inventory.getStatus()));
        InventoryStatuses inventoryStatus = null;

        if (needToSave) {
            inventoryStatus = new InventoryStatuses(inventory, inventory.getStatus(), new Date());
        }

        if (isNew) {
            inventoryDAO.create(inventory);
        } else {
            inventoryDAO.update(inventory);
        }
        if (needToSave) {
            inventoryStatusesDAO.create(inventoryStatus);
        }

    }

    @Override
    public void delete(Inventory inventory) {
        deleteStatuses(inventory);
        deleteInventory(inventory);
    }

    @Override
    @Transactional(readOnly = true)
    public boolean isNumberNotExist(Inventory inventory) {
        return inventoryDAO.isNumberNotExist(inventory.getId(), inventory.getNumber());
    }

    @Override
    public boolean isNumberNotExist(Integer id, String number) {
        return inventoryDAO.isNumberNotExist(id, number);
    }

    @Override
    @Transactional(readOnly = true)
    public List<InventoryStatuses> getStatusesList(Inventory inventory) {
        return inventoryStatusesDAO.getListByInventory(inventory);
    }

    @Override
    public Date getLastStatusDate(Inventory inventory) {
        return inventoryStatusesDAO.getLastDate(inventory);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    private void deleteStatuses(Inventory inventory) {
        inventoryStatusesDAO.deleteByInventory(inventory);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    private void deleteInventory(Inventory inventory) {
        inventoryDAO.delete(inventory);
    }

    private InventoryDTO createInventoryDTO(Inventory inventory) throws PortalException, SystemException {
        InventoryDTO inventoryDTO = new InventoryDTO(inventory);
        inventoryDTO.setDateOfStatus(new SimpleDateFormat("MM-dd-yyyy HH:mm:ss").format(inventoryStatusesDAO.getLastDate(inventory)));
        return inventoryDTO;
    }

    private List<InventoryDTO> createInventoryDTOList(List<Inventory> inventories) throws PortalException, SystemException {
        List<InventoryDTO> inventoryDTOs = new ArrayList<InventoryDTO>(inventories.size());
        for(Inventory inventory:inventories) {
            inventoryDTOs.add(createInventoryDTO(inventory));
        }
        return inventoryDTOs;
    }

}

package com.aimprosoft.tekcontrol.assets.service;

import com.aimprosoft.tekcontrol.assets.dao.Order;
import com.aimprosoft.tekcontrol.assets.domain.Status;

import java.util.List;
import java.util.Set;

public interface StatusService {

    Set<Status> getSet();

    List<Status> getList();

    List<Status> getList(int start, int end, String orderField, Order order);

    List<Status> getList(int start, int end, String orderField, String order);

    int getListCount();

    Status getById(int id);

    void save(Status status);

    boolean delete(Status status);

    boolean isNameNotExist(Status status);

    boolean isNameNotExist(Integer id, String name);

}

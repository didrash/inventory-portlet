package com.aimprosoft.tekcontrol.assets.service.validator;

import com.aimprosoft.tekcontrol.assets.domain.Inventory;
import com.aimprosoft.tekcontrol.assets.service.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class InventoryInfoValidator implements Validator {

    @Autowired
    private InventoryService inventoryService;

    @Override
    public boolean supports(Class<?> clazz) {
        return Inventory.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Inventory inventory = (Inventory) target;
        if (!inventoryService.isNumberNotExist(inventory)) {
            errors.rejectValue("number", "this number has already existed", "this number has already existed");
        }
    }
}

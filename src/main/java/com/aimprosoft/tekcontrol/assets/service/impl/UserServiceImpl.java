package com.aimprosoft.tekcontrol.assets.service.impl;

import com.aimprosoft.tekcontrol.assets.service.UserService;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserServiceUtil;
import com.liferay.portal.util.PortalUtil;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Override
    public List<User> getList() throws SystemException, PortalException {
        return  UserServiceUtil.getCompanyUsers(PortalUtil.getDefaultCompanyId(), QueryUtil.ALL_POS, QueryUtil.ALL_POS);
    }

}

package com.aimprosoft.tekcontrol.assets.service.impl;

import com.aimprosoft.tekcontrol.assets.dao.Order;
import com.aimprosoft.tekcontrol.assets.dao.StatusDAO;
import com.aimprosoft.tekcontrol.assets.domain.Status;
import com.aimprosoft.tekcontrol.assets.service.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class StatusServiceImpl implements StatusService {

    @Autowired
    private StatusDAO statusDAO;

    @Override
    @Transactional(readOnly = true)
    public Set<Status> getSet() {
        return new HashSet<Status>(statusDAO.getList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<Status> getList() {
        return statusDAO.getList();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Status> getList(int start, int end, String orderField, Order order) {
        return statusDAO.getList(start, end, orderField, order);
    }

    @Override
    public List<Status> getList(int start, int end, String orderField, String order) {
        return getList(start, end, orderField, Order.getOrder(order));
    }

    @Override
    @Transactional(readOnly = true)
    public int getListCount() {
        return statusDAO.getListCount();
    }

    @Override
    @Transactional(readOnly = true)
    public Status getById(int id) {
        return statusDAO.getById(id);
    }

    @Override
    @Transactional
    public void save(Status status) {
        if(status.isNew()){
            statusDAO.create(status);
        }else {
            statusDAO.update(status);
        }
    }

    @Override
    @Transactional
    public boolean delete(Status status) {
        if(statusDAO.isDeletable(status)){
            statusDAO.deleteById(status.getId());
            return true;
        }
        return false;
    }

    @Override
    @Transactional(readOnly = true)
    public boolean isNameNotExist(Status status) {
        return statusDAO.isNameNotExist(status.getId(), status.getName());
    }

    @Override
    public boolean isNameNotExist(Integer id, String name) {
        return statusDAO.isNameNotExist(id, name);
    }

}

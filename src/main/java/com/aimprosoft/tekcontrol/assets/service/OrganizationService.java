package com.aimprosoft.tekcontrol.assets.service;

import com.aimprosoft.tekcontrol.assets.dto.OrganizationDTO;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.Organization;

import java.util.List;

/**
 * Created on 7/1/14.
 */
public interface OrganizationService {

    List<Organization> getOrganizationList() throws SystemException;

    List<Organization> getLocationList() throws SystemException;

    List<Organization> getLocationList(Long parentOrganizationId) throws SystemException;

    List<OrganizationDTO> getLocationDTOList(Long parentOrganizationId) throws SystemException;

    List<Organization> getList() throws SystemException;

    List<OrganizationDTO> getDTOList() throws SystemException;

    Organization getById(long id) throws SystemException, PortalException;

    long[] getNestedIds(long organizationId);

}

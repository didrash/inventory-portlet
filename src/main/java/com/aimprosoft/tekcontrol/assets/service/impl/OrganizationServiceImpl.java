package com.aimprosoft.tekcontrol.assets.service.impl;

import com.aimprosoft.tekcontrol.assets.dto.OrganizationDTO;
import com.aimprosoft.tekcontrol.assets.service.OrganizationService;
import com.aimprosoft.tekcontrol.assets.service.OrganizationType;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.Organization;
import com.liferay.portal.service.OrganizationLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class OrganizationServiceImpl implements OrganizationService {

    private static final Logger _log = LoggerFactory.getLogger(OrganizationServiceImpl.class);

    @Override
    public List<Organization> getOrganizationList() throws SystemException {
        return getList(OrganizationType.ORGANIZATION);
    }

    @Override
    public List<Organization> getLocationList() throws SystemException {
        return getList(OrganizationType.LOCATION);
    }

    @Override
    public List<Organization> getLocationList(Long parentOrganizationId) throws SystemException {
        if (parentOrganizationId == null)
            return Collections.emptyList();
        else {
            return splitList(getFullList(parentOrganizationId), OrganizationType.LOCATION);
        }
    }

    @Override
    public List<OrganizationDTO> getLocationDTOList(Long parentOrganizationId) throws SystemException {
        return getDTOFromModelList(getLocationList(parentOrganizationId));
    }

    @Override
    public List<Organization> getList() throws SystemException {
        return getFullList();
    }

    @Override
    public List<OrganizationDTO> getDTOList() throws SystemException {
        return getDTOFromModelList(getFullList());
    }

    private List<OrganizationDTO> getDTOFromModelList(List<Organization> organizationList){
        List<OrganizationDTO> fullDtoList = new ArrayList<OrganizationDTO>(organizationList.size());
        for (Organization organization : organizationList) {
            fullDtoList.add(new OrganizationDTO(organization));
        }
        return fullDtoList;
    }

    @Override
    public Organization getById(long id) throws SystemException, PortalException {
        return OrganizationLocalServiceUtil.getOrganization(id);
    }

    @Override
    public long[] getNestedIds(long organizationId) {

        List<Organization> organizationList = Collections.emptyList();
        try {
            organizationList = OrganizationLocalServiceUtil.getSuborganizations(PortalUtil.getDefaultCompanyId(), organizationId);
        } catch (SystemException e) {
            _log.error("Occurred in get liferay organizations method", e);
        }
        long[] organizationIds = new long[organizationList.size() + 1];

        for (int i = 0; i < organizationList.size(); i++) {
            organizationIds[i] = organizationList.get(i).getOrganizationId();
        }

        organizationIds[organizationList.size()] = organizationId;
        return organizationIds;

    }

    private List<Organization> getList(OrganizationType type) throws SystemException {

        return splitList(getFullList(), type);

    }

    private List<Organization> splitList(List<Organization> organizationList, OrganizationType type) throws SystemException {

        String equalsString = type.toString();
        ArrayList<Organization> returningList = new ArrayList<Organization>(organizationList.size());

        for (Organization organization : organizationList) {
            if (organization.getType().equals(equalsString)) {
                returningList.add(organization);
            }
        }

        returningList.trimToSize();
        return returningList;
    }


    private List<Organization> getFullList() throws SystemException {

        List<Organization> allowedOrgs = new ArrayList<Organization>();
        List<Organization> secondLvlOrgs = new ArrayList<Organization>();

        List<Organization> orgs = new ArrayList<Organization>(OrganizationLocalServiceUtil.getOrganizations(
                QueryUtil.ALL_POS, QueryUtil.ALL_POS));

        for (Organization org : orgs) {
            if (org.isRoot()) {
                allowedOrgs.add(org);
            } else {
                secondLvlOrgs.add(org);
            }
        }
        allowedOrgs.addAll(secondLvlOrgs);

        return allowedOrgs;

    }

    private List<Organization> getFullList(Long parentOrganizationId) throws SystemException {
        List<Organization> organizationList = getFullListRecursion(parentOrganizationId);
        Set<Organization> organizationSet = new HashSet<Organization>(organizationList);
        return new ArrayList<Organization>(organizationSet);
    }

    private List<Organization> getFullListRecursion(Long parentOrganizationId) throws SystemException {

        List<Organization> organizationList = new ArrayList<Organization>(OrganizationLocalServiceUtil.getSuborganizations(PortalUtil.getDefaultCompanyId(), parentOrganizationId));
        List<Organization> storageList = new ArrayList<Organization>();
        for(Organization organization:organizationList){
            List<Organization> subOrganizationList = getFullListRecursion(organization.getOrganizationId());
            storageList.addAll(subOrganizationList);
        }
        organizationList.addAll(storageList);
        return organizationList;
    }

}

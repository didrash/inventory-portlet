package com.aimprosoft.tekcontrol.assets.service.formatter;

import com.aimprosoft.tekcontrol.assets.domain.Status;
import com.aimprosoft.tekcontrol.assets.service.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import java.text.ParseException;
import java.util.Collection;
import java.util.Locale;

public class StatusFormatter implements Formatter<Status> {

    @Autowired
    private StatusService statusService;

    @Override
    public Status parse(String text, Locale locale) throws ParseException {
        Collection<Status> statuses = this.statusService.getList();
        for (Status status : statuses) {
            if (status.getName().equals(text)) {
                return status;
            }
        }
        throw new ParseException("type not found: " + text, 0);
    }

    @Override
    public String print(Status object, Locale locale) {
        return object.getName();
    }

}

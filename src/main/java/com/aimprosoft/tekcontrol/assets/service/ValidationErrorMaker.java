package com.aimprosoft.tekcontrol.assets.service;

import org.springframework.ui.Model;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import java.util.List;

/**
 * Created on 4/17/14.
 */
public class ValidationErrorMaker {

    public static void makeErrors(Model model, List<ObjectError> errors){

        for(ObjectError objectError:errors){
            FieldError fieldError = (FieldError) objectError;
            model.addAttribute("error"+fieldError.getField(), fieldError.getDefaultMessage());
            model.addAttribute("error"+fieldError.getField()+"flag", true);
        }

    }

}

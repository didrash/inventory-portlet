<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet" %>



<portlet:renderURL var="inventoryListURL">
    <portlet:param name="action" value="inventoryList"></portlet:param>
    <portlet:param name="organization_id" value="<%= null %>"></portlet:param>    
</portlet:renderURL>

<portlet:renderURL var="inventoryAddURL">
    <portlet:param name="action" value="inventoryAdd"></portlet:param>
    <portlet:param name="way" value="add"></portlet:param>
</portlet:renderURL>


<portlet:renderURL var="statusListURL">
    <portlet:param name="action" value="statusList"></portlet:param>
</portlet:renderURL>

<portlet:renderURL var="statusAddURL">
    <portlet:param name="action" value="statusAdd"></portlet:param>
</portlet:renderURL>

<div id="page-wrap">
    <P></P>

    <div id="content">
        <ul id="menu">
            <li class="inventories">
                <a href="${inventoryListURL}">Inventories</a>
                <ul>
                    <li><a href="${inventoryListURL}">List</a></li>
                    <li><a href="${inventoryAddURL}">Add</a></li>
                </ul>
            </li>
            <li class="statuses">
                <a href="${statusListURL}">Statuses</a>
                <ul>
                    <li><a href="${statusListURL}">List</a></li>
                    <li><a href="${statusAddURL}">Add</a></li>
                </ul>
            </li>

        </ul>
    </div>
</div>
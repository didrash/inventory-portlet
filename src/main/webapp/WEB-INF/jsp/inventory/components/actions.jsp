<%@ page import="com.aimprosoft.tekcontrol.assets.domain.Status" %>
<%@ page import="com.liferay.portal.kernel.dao.search.ResultRow" %>
<%@ page import="com.liferay.portal.kernel.dao.search.SearchContainer" %>
<%@ page import="com.liferay.portal.kernel.util.WebKeys" %>
<%@ page import="com.aimprosoft.tekcontrol.assets.dto.InventoryDTO" %>
<%@ include file="../../init.jsp" %>

<%

    SearchContainer searchContainer = (SearchContainer) request.getAttribute("liferay-ui:search:searchContainer");
    String redirect = searchContainer.getIteratorURL().toString();
    ResultRow row = (ResultRow) request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
    InventoryDTO entry = (InventoryDTO) row.getObject();

%>

<liferay-ui:icon-menu id='<%="icon_menu_"+entry.getId() %>'>

    <portlet:renderURL var="viewURL">
        <portlet:param name="action" value="inventoryView"></portlet:param>
        <portlet:param name="id" value="<%= String.valueOf(entry.getId())%>"></portlet:param>
    </portlet:renderURL>

    <portlet:renderURL var="editURL">
        <portlet:param name="action" value="inventoryEdit"></portlet:param>
        <portlet:param name="id" value="<%= String.valueOf(entry.getId().toString())%>"></portlet:param>
    </portlet:renderURL>

    <portlet:actionURL var="deleteURL">
        <portlet:param name="action" value="inventoryDelete"></portlet:param>
        <portlet:param name="id" value="<%= String.valueOf(entry.getId().toString())%>"></portlet:param>
    </portlet:actionURL>

    <liferay-ui:icon url="<%=viewURL%>" label="view" image="view"/>
    <liferay-ui:icon url="<%=editURL%>" label="edit" image="edit"/>
    <liferay-ui:icon-delete url="<%=deleteURL%>" />

</liferay-ui:icon-menu>

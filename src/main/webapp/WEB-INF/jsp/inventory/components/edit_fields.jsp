<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="ns"><portlet:namespace/></c:set>

<c:set var="cssGroupName" value="control-group ${errornameflag ? 'error' : '' }"/>
<c:set var="cssGroupNumber" value="control-group ${errornumberflag ? 'error' : '' }"/>
<c:set var="cssGroupStatus" value="control-group ${errorstatusflag ? 'error' : '' }"/>

<input type="hidden" name="${ns}id" id="id" value="${inventory.id}">

<table>

    <div class="form-group">
        <label class="col-lg-3 control-label">Name</label>

        <div class="col-lg-5">
            <input type="text" class="form-control" name="name"/>
        </div>
    </div>

    <tr>
        <td>
            <div class="${cssGroupName}">
                <label class="control-label">Name</label>
                <div class="control-group">
                    <input type="text" name="name" id="name" value="${inventory.name}">
                    <span class="help-inline">${errorname}</span>
                </div>
            </div>
        </td>
        <td rowspan="3">
            <div class="control-group">
                <label class="control-label">Notes</label>
                <div class="controls">
                    <textarea type="text" aria-multiline="true" style="width: 300px; height: 200px" name="${ns}notes"
                              id="notes">
                        ${inventory.notes}
                    </textarea>
                </div>
            </div>
        </td>
    </tr>

    <tr>
        <td>
            <div class="${cssGroupNumber}">
                <label class="control-label">Number</label>

                <div class="controls">
                    <input type="text" name="number" id="number" value="${inventory.number}">
                    <span class="help-inline">${errornumber}</span>
                </div>
            </div>
        </td>
    </tr>

    <tr>
        <td>
            <div class="control-group">
                <label class="control-label">Organization</label>
                <select name="$organization_id">
                    <option
                    ${inventory.organization_id == null ? 'selected="selected"' : ''}>
                    </option>
                    <c:forEach var="organization" items="${organizationList}">
                        <c:set var="parentOrganization" value='${organization.parentOrganization.name}'/>
                        <option value="<c:out value='${organization.organizationId}'/>"
                            ${inventory.organization_id == organization.organizationId ? 'selected="selected"' : ''}>
                                ${organization.name}
                            <c:if test="${!empty parentOrganization}">
                                (${parentOrganization})
                            </c:if>
                        </option>
                    </c:forEach>
                </select>
            </div>
        </td>
    </tr>

    <tr>
        <td>
            <div class="control-group">
                <label class="control-label">Assigned User</label>
                <select name="$user_id">
                    <option
                    ${inventory.user_id == null ? 'selected="selected"' : ''}>
                    </option>
                    <c:forEach var="user" items="${userList}">
                        <option value="<c:out value='${user.userId}'/>"
                            ${inventory.user_id == user.userId ? 'selected="selected"' : ''}>
                            <c:out value='${user.fullName}'/>
                        </option>
                    </c:forEach>
                </select>
            </div>
        </td>
        <td rowspan="3">
            <div class="control-group">
                <label class="control-label">Statuses History</label>

                <div class="controls">
                    <jsp:include page="statuses_history.jsp"></jsp:include>
                </div>
            </div>
        </td>
    </tr>

    <tr>
        <td>
            <div class="control-group">
                <label class="control-label">Location</label>
                <select name="$location_id">
                    <option
                    ${inventory.location_id == null ? 'selected="selected"' : ''}>
                    </option>
                    <c:forEach var="location" items="${locationList}">
                        <c:set var="parentOrganization" value='${location.parentOrganization.name}'/>
                        <option value="<c:out value='${location.organizationId}'/>"
                            ${inventory.location_id == location.organizationId ? 'selected="selected"' : ''}>
                            <c:out value='${location.name}'/>
                            <c:if test="${!empty parentOrganization}">
                                (${parentOrganization})
                            </c:if>
                        </option>
                    </c:forEach>
                </select>
            </div>
        </td>
    </tr>

    <tr>
        <td>
            <div class="${cssGroupStatus}">
                <label class="control-label">Status</label>
                <select name="status">
                    <option value="${null}"
                    ${inventory.status == null ? 'selected="selected"' : ''}>
                    </option>
                    <c:forEach var="status" items="${statusList}">
                        <option value="<c:out value='${status.id}'/>"
                            ${inventory.status.id == status.id ? 'selected="selected"' : ''}>
                            <c:out value='${status.name}'/>
                        </option>
                    </c:forEach>
                </select>
                <span class="help-inline">${errorstatus}</span>
            </div>
        </td>
    </tr>

</table>
</div>

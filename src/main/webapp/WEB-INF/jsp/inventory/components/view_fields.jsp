<input type="hidden" name="${ns}id" id="id" value="${inventoryDTO.id}">
<table class="table table-striped" style="width:600px;">
    <tr>
        <td>name:</td>
        <td>
            ${inventoryDTO.name}
        </td>
    </tr>
    <tr>
        <td>number:</td>
        <td>
            ${inventoryDTO.number}
        </td>
    </tr>
    <tr>
        <td>Organization:</td>
        <td>
            ${inventoryDTO.organization}
        </td>
    </tr>
    <tr>
        <td>User:</td>
        <td>
            ${inventoryDTO.user}
        </td>
    </tr>
    <tr>
        <td>Location:</td>
        <td>
            ${inventoryDTO.location}
        </td>
    </tr>
    <tr>
        <td>Status:</td>
        <td>
            ${inventoryDTO.status}
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <jsp:include page="statuses_history.jsp"></jsp:include>
        </td>
    </tr>

</table>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="control-group">
    <label class="control-label">Statuses History</label>
    <div class="controls">
        <table class="table table-striped">
            <th>
                Status
            </th>
            <th>
                Date
            </th>
            <c:forEach var="inventoryStatus" items="${inventoryStatusesList}">
                <fmt:formatDate value="${inventoryStatus.date}" var="formattedDate"
                                type="date" pattern="MM-dd-yyyy HH:mm:ss"/>
                <tr>
                    <td>
                            ${inventoryStatus.status.name}
                    </td>
                    <td>
                            ${formattedDate}
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>
</div>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="test" tagdir="/WEB-INF/tags" %>
<jsp:include page="../fragments/headTag.jsp"/>
<jsp:include page="../menu.jsp"/>

<portlet:renderURL var="editURL">
    <portlet:param name="action" value="inventoryEdit"/>
</portlet:renderURL>

<portlet:actionURL var="deleteURL">
    <portlet:param name="action" value="inventoryDelete"></portlet:param>
</portlet:actionURL>

<form id="editForm" action="" method="post">
    <jsp:include page="components/view_fields.jsp"/>
    <input id="editSubmit" type="submit" value="edit" class="btn btn-success">
    <input id="deleteSubmit" type="submit" value="delete" class="btn btn-danger">
</form>

<script type="text/javascript">
    var varSaveURL= "${editURL}";
    var varDeleteURL = "${deleteURL}";
</script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/view_common.js"></script>
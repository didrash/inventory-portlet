<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="test" tagdir="/WEB-INF/tags" %>

<jsp:include page="../fragments/headTag.jsp"/>

<jsp:include page="../menu.jsp"/>

<portlet:actionURL var="saveURL">
    <portlet:param name="action" value="inventorySave"></portlet:param>
</portlet:actionURL>

<portlet:resourceURL var="isNumberValidCheckURL" id="isNumberValidCheckURL"/>
<portlet:resourceURL var="getLocationsByParentOrganizationIdURL" id="getLocationsByParentOrganizationIdURL"/>

<div class="container">

    <form class="form-horizontal" id="inventoryEditForm" name="inventoryEditForm" action="${saveURL}" method="post">

        <input type="hidden" name="id" value="${inventory.id}">

        <div class="row">
            <div class="col-xs-6">
                <div class="form-group required">
                    <label class="col-lg-5 control-label">Name</label>

                    <div class="col-lg-9">
                        <input type="text" class="form-control" id="name" name="name" value="${inventory.name}"/>
                    </div>
                </div>
                <div class="form-group required">
                    <label class="col-lg-5 control-label">Number</label>

                    <div class="col-lg-9">
                        <input type="text" class="form-control" name="number" value="${inventory.number}"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-5 control-label">Organization</label>

                    <div class="col-lg-9">
                        <select class="form-control" name="organization_id" id="organization_id">
                            <option value="${null}" ${inventory.organization_id == null ? 'selected="selected"' : ''}>
                                Choose organization
                            </option>
                            <c:forEach var="organization" items="${organizationList}">
                                <c:set var="parentOrganization"
                                       value='${organization.parentOrganization.name}'/>
                                <option value="<c:out value='${organization.organizationId}'/>"
                                    ${inventory.organization_id == organization.organizationId ? 'selected="selected"' : ''}>
                                        ${organization.name}
                                    <c:if test="${!empty parentOrganization}">
                                        (${parentOrganization})
                                    </c:if>
                                </option>
                            </c:forEach>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-5 control-label">Assigned user</label>

                    <div class="col-lg-9">
                        <select class="form-control" name="user_id" id="user_id">
                            <option value="${null}" ${inventory.user_id == null ? 'selected="selected"' : ''}>Choose
                                user
                            </option>
                            <c:forEach var="user" items="${userList}">
                                <option value="<c:out value='${user.userId}'/>"
                                    ${inventory.user_id == user.userId ? 'selected="selected"' : ''}>
                                    <c:out value='${user.fullName}'/>
                                </option>
                            </c:forEach>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-5 control-label">Location</label>

                    <div class="col-lg-9">
                        <select class="form-control" name="location_id" id="location_id">
                            <option value="${null}" ${inventory.location_id == null ? 'selected="selected"' : ''}>Choose
                                location
                            </option>
                            <c:forEach var="location" items="${locationList}">
                                <c:set var="parentOrganization"
                                       value='${location.parentOrganization.name}'/>
                                <option value="<c:out value='${location.organizationId}'/>"
                                    ${inventory.location_id == location.organizationId ? 'selected="selected"' : ''}>
                                    <c:out value='${location.name}'/>
                                    <c:if test="${!empty parentOrganization}">
                                        (${parentOrganization})
                                    </c:if>
                                </option>
                            </c:forEach>
                        </select>
                    </div>
                </div>

                <div class="form-group required">
                    <label class="col-lg-5 control-label">Status</label>

                    <div class="col-lg-9">
                        <select class="form-control" name="status">
                            <option value="${null}" ${inventory.status == null ? 'selected="selected"' : ''}>
                                Choose
                                status
                            </option>
                            <c:forEach var="status" items="${statusList}">
                                <option value="<c:out value='${status.id}'/>"
                                    ${inventory.status.id == status.id ? 'selected="selected"' : ''}>
                                    <c:out value='${status.name}'/>
                                </option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-xs-6">
                <div class="form-group">
                    <label class="col-lg-5 control-label">Notes</label>

                    <div class="col-lg-9">
                        <textarea class="form-control" type="text" aria-multiline="true"
                                  style="width: 300px; height: 200px" name="notes"
                                  id="notes">
                            ${inventory.notes}
                        </textarea>
                    </div>
                </div>

                <div class="table table-striped">
                    <jsp:include page="components/statuses_history.jsp"></jsp:include>
                </div>

            </div>
        </div>

        <div class="row-fluid">
            <div class="col-xs-12">
                <div class="form-group">
                    <div class="col-lg-9">
                        <button type="submit" class="btn btn-default">Save</button>
                    </div>
                </div>
            </div>
        </div>

    </form>

</div>

</div>

<script type="text/javascript">
    var isNumberValidCheckURL = "${isNumberValidCheckURL}";
    var getLocationsByParentOrganizationIdURL = "${getLocationsByParentOrganizationIdURL}";
    var id = "${inventory.id}";
</script>

<script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/js/inventory/inventory_edit.js"></script>



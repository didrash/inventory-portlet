<%@ page import="com.liferay.portal.kernel.dao.search.SearchContainer" %>
<%@page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="datatables" uri="http://github.com/dandelion/datatables" %>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui" %>

<%@ include file="../init.jsp" %>
<jsp:include page="../fragments/headTag.jsp"/>
<jsp:include page="../menu.jsp"/>

<h2>Inventories</h2>

<portlet:renderURL var="inventoryAddURL">
    <portlet:param name="action" value="inventoryAdd"/>
</portlet:renderURL>

<portlet:renderURL var="listURL">
    <portlet:param name="action" value="inventoryList"/>
</portlet:renderURL>

<portlet:resourceURL var="startURL" id="startURL"/>

<div id='jqxWidget'>
</div>
<div>
    <h3>${condition}</h3>
</div>

<a href="${inventoryAddURL}" class="btn btn-success">Add New Inventory</a>

<liferay-ui:search-container searchContainer="${searchContainer}">
    <!-- In order to get the total results and also to display no. of data -->
    <liferay-ui:search-container-results results="<%=searchContainer.getResults() %>"
                                         total="<%=searchContainer.getTotal() %>">
    </liferay-ui:search-container-results>
    <!-- Display Row with Name and Description as columns -->
    <liferay-ui:search-container-row className="com.aimprosoft.tekcontrol.assets.dto.InventoryDTO" keyProperty="id"
                                     modelVar="inventory">
        <liferay-ui:search-container-column-text name="Name" property="name" orderable="<%= true %>"
                                                 orderableProperty="name"/>
        <liferay-ui:search-container-column-text name="Number" property="number" orderable="<%= true %>"
                                                 orderableProperty="number"/>
        <liferay-ui:search-container-column-text name="Assigned user" property="user"/>
        <liferay-ui:search-container-column-text name="Organization" property="organization"/>
        <liferay-ui:search-container-column-text name="Location" property="location"/>
        <liferay-ui:search-container-column-text name="Status" property="status" orderable="<%= true %>"
                                                 orderableProperty="status"/>
        <liferay-ui:search-container-column-text name="Date of status" property="dateOfStatus"/>
        <liferay-ui:search-container-column-jsp
                path="/WEB-INF/jsp/inventory/components/actions.jsp" align="right"/>
    </liferay-ui:search-container-row>
    <!-- Iterating the Results -->
    <liferay-ui:search-iterator paginate="true" searchContainer="${searchContainer}"/>
</liferay-ui:search-container>

<%

    SearchContainer searchContainer = (SearchContainer) request.getAttribute("liferay-ui:search:searchContainer");
    session.setAttribute("searchContainer_delta", searchContainer.getDelta());

%>


<script type="text/javascript">
    var startURL = "${startURL}";
    var listURL = "${listURL}";
</script>

<script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/js/inventory/inventory_list.js"></script>


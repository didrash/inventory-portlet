<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<spring:url value="/resources/css/bootstrap.css" var="bootstrapCss"/>
<link href="${bootstrapCss}" rel="stylesheet"/>

<spring:url value="/resources/css/bootstrap.css.map" var="bootstrapCssMap"/>
<link href="${bootstrapCssMap}" rel="stylesheet"/>

<spring:url value="/resources/css/bootstrap-theme.css" var="bootstrapCssTheme"/>
<link href="${bootstrapCssTheme}" rel="stylesheet"/>

<spring:url value="/resources/css/bootstrap-theme.css.map" var="bootstrapCssThemeMap"/>
<link href="${bootstrapCssThemeMap}" rel="stylesheet"/>

<spring:url value="/resources/css/bootstrapValidator.min.css" var="bootstrapCssValidator"/>
<link href="${bootstrapCssValidator}" rel="stylesheet"/>

<spring:url value="/resources/css/menu_style.css" var="petclinicCss"/>
<link href="${petclinicCss}" rel="stylesheet"/>

<spring:url value="/resources/css/jqx.metro.css" var="petclinicCss"/>
<link href="${petclinicCss}" rel="stylesheet"/>

<spring:url value="/resources/css/jqx.base.css" var="petclinicCss"/>
<link href="${petclinicCss}" rel="stylesheet"/>

<spring:url value="/webjars/jquery/2.0.3/jquery.js" var="jQuery"/>
<script src="${jQuery}"></script>

<spring:url value="/webjars/jquery-ui/1.10.3/ui/jquery.ui.core.js" var="jQueryUiCore"/>
<script src="${jQueryUiCore}"></script>

<spring:url value="/webjars/jquery-ui/1.10.3/ui/jquery.ui.datepicker.js" var="jQueryUiDatePicker"/>
<script src="${jQueryUiDatePicker}"></script>

<spring:url value="/webjars/jquery-ui/1.10.3/themes/base/jquery-ui.css" var="jQueryUiCss"/>
<link href="${jQueryUiCss}" rel="stylesheet"/>

<!-- JS -->
<%--<spring:url value="/resources/js/jquery-2.0.2.js" var="jquery_2_0_2"/>
<script type="text/javascript" src="${jquery_2_0_2}"></script>--%>

<spring:url value="/resources/js/jquery-1.11.1.min.js" var="jquerysadasda"/>
<script type="text/javascript" src="${jquerysadasda}"></script>

<spring:url value="/resources/js/jqx-all.js" var="jqx"/>
<script type="text/javascript" src="${jqx}"></script>

<spring:url value="/resources/js/jquery-ui-1.10.4.js" var="jquery_ui"/>
<script type="text/javascript" src="${jquery_ui}"></script>

<spring:url value="/resources/js/jquery.validate.js" var="jquery_validate"/>
<script type="text/javascript" src="${jquery_validate}"></script>

<spring:url value="/resources/js/bootstrapValidator.min.js" var="bootstrapValidatorJS"/>
<script type="text/javascript" src="${bootstrapValidatorJS}"></script>

<spring:url value="/resources/js/language/en_US.js" var="en_US"/>
<script type="text/javascript" src="${en_US}"></script>

<%@page pageEncoding="UTF-8"%>
<%@ include file="init.jsp"%>
<jsp:include page="fragments/headTag.jsp"/>

<spring:url value="/resources/images/error.png" var="error_image"/>
<img src="${error_image}">

<p>
ERROR! message : <c:out value="${exception.message}"/>
</p>

<p>
StackTrace : <c:out value="${stacktrace}"/>
</p>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../init.jsp" %>
<c:set var="ns"><portlet:namespace/></c:set>
<jsp:include page="../fragments/headTag.jsp"/>

<jsp:include page="../menu.jsp"/>

<portlet:renderURL var="editURL">
    <portlet:param name="action" value="statusEdit"/>
</portlet:renderURL>

<portlet:actionURL var="deleteURL">
    <portlet:param name="action" value="statusDelete"></portlet:param>
</portlet:actionURL>

<div class="control-group has-error">
    <span class="help-inline">${errorOnDelete}</span>
</div>

<form id="editForm" action="" method="post">
    <jsp:include page="components/view_fields.jsp"/>
    <input id="editSubmit" type="submit" value="edit" class="btn btn-success">
    <input id="deleteSubmit" type="submit"  value="delete" class="btn btn-danger">
</form>

<script type="text/javascript">
    var varSaveURL= "${editURL}";
    var varDeleteURL = "${deleteURL}";
</script>

<script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/js/view_common.js"></script>
<%@page pageEncoding="UTF-8" %>
<%@ page import="com.aimprosoft.tekcontrol.assets.domain.Status" %>
<%@page import="java.util.List" %>

<%@ include file="../init.jsp" %>
<jsp:include page="../fragments/headTag.jsp"/>
<jsp:include page="../menu.jsp"/>

<h2>Statuses</h2>

<portlet:renderURL var="statusAddURL">
    <portlet:param name="action" value="statusAdd"></portlet:param>
</portlet:renderURL>

<portlet:actionURL var="deleteURL">
    <portlet:param name="action" value="statusDelete"></portlet:param>
</portlet:actionURL>

<a href="${statusAddURL}" class="btn btn-success">Add New Status</a>

<liferay-ui:search-container searchContainer="${searchContainer}">
    <!-- In order to get the total results and also to display no. of data -->
    <liferay-ui:search-container-results results="<%=searchContainer.getResults() %>"
                                         total="<%=searchContainer.getTotal() %>">
    </liferay-ui:search-container-results>
    <!-- Display Row with Name and Description as columns -->
    <liferay-ui:search-container-row className="com.aimprosoft.tekcontrol.assets.domain.Status" keyProperty="id"
                                     modelVar="status">
        <liferay-ui:search-container-column-text name="Name" property="name" orderable="<%= true %>" orderableProperty="name"/>
        <liferay-ui:search-container-column-jsp
                path="/WEB-INF/jsp/status/components/actions.jsp" align="right"/>

    </liferay-ui:search-container-row>
    <!-- Iterating the Results -->
    <liferay-ui:search-iterator paginate="true" searchContainer="${searchContainer}"  />
</liferay-ui:search-container>

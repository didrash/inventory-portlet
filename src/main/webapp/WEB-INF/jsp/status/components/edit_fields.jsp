<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="ns"><portlet:namespace/></c:set>
<c:set var="cssGroup" value="control-group ${errornameflag ? 'error' : '' }"/>
<input type="hidden" name="${ns}id" id="id" value="${status.id}">

<div class="${cssGroup}">

    <tr>
        <td>
            <label class="control-label">Name</label>

            <div class="controls">
                <input type="text" name="${ns}name" id="name" value="${status.name}">
                <span class="help-inline">${errorname}</span>
            </div>
        </td>
    </tr>


</div>

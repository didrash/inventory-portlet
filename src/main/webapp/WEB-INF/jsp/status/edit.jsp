<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="test" tagdir="/WEB-INF/tags" %>
<c:set var="ns"><portlet:namespace/></c:set>
<jsp:include page="../fragments/headTag.jsp"/>

<jsp:include page="../menu.jsp"/>

<portlet:actionURL var="saveURL">
    <portlet:param name="action" value="statusSave"></portlet:param>
</portlet:actionURL>

<portlet:resourceURL var="isNameValidCheckURL" id="isNameValidCheckURL"/>

<div class="container">

    <form class="form-horizontal" id="statusEditForm" name="statusEditForm" action="${saveURL}" method="post">
        <input type="hidden" name="id" value="${status.id}">

        <div class="row">
            <div class="col-xs-6">
                <div class="form-group required">
                    <label class="col-lg-5 control-label">Name</label>
                    <div class="col-lg-9">
                        <input type="text" class="form-control" name="name" value="${status.name}"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row-fluid">
            <div class="col-xs-6">
                <div class="form-group">
                    <div class="col-lg-9">
                        <button type="submit" class="btn btn-default">Save</button>
                    </div>
                </div>
            </div>
        </div>

    </form>

</div>

<script type="text/javascript">
    var isNameValidCheckURL = "${isNameValidCheckURL}";
    var id = "${status.id}";
</script>

<script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/js/status/status_edit.js"></script>
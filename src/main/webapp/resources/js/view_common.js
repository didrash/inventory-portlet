(function ($, W, D) {

    $(D).ready(function () {


        $("#editSubmit").click(function () {
            $('#editForm').attr('action', varSaveURL);
        });

        $("#deleteSubmit").click(function () {
            $('#editForm').attr('action', varDeleteURL);
        });

    });

})(jQuery, window, document);

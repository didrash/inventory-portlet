(function ($, W, D) {

    $(D).ready(function () {

        $.getJSON(startURL, function (json) {

            var organization = json["dtoList"];

            // prepare the data
            var source =
            {
                datatype: "json",
                datafields: [
                    { name: 'id' },
                    { name: 'parentid' },
                    { name: 'name' },
                    { name: 'subMenuWidth' }
                ],
                id: 'id',
                localdata: organization
            };
            // create data adapter.
            var dataAdapter = new $.jqx.dataAdapter(source);
            // perform Data Binding.
            dataAdapter.dataBind();
            // get the menu items. The first parameter is the item's id. The second parameter is the parent item's id. The 'items' parameter represents
            // the sub items collection name. Each jqxTree item has a 'label' property, but in the JSON data, we have a 'text' field. The last parameter
            // specifies the mapping between the 'text' and 'label' fields.
            var records = dataAdapter.getRecordsHierarchy('id', 'parentid', 'items', [
                { name: 'name', map: 'label'}
            ]);
            $('#jqxWidget').jqxMenu({ source: records, width: '600px', height: '35px', theme: 'energyblue'});
            $("#jqxWidget").on('itemclick', function (event) {

                $("#inventory_table").empty();

                var id = event.args.id;
                window.location = listURL + "&organization_id=" + id;

            });
        });

    });

})(jQuery, window, document);
(function ($, W, D) {

    $(D).ready(function () {

        var createOptionsOnLocationAndUserByChoosingOrganization = function () {

            var organization_id = $("#organization_id").val();

            $.ajax({
                url: getLocationsByParentOrganizationIdURL,
                dataType: 'json',
                async: false,
                data: {id: organization_id},
                success: function (json) {

                    var locations = json["dtoList"];

                    var newLocationOptions = new Object();

                    $.each($.parseJSON(locations), function (index, element) {
                        newLocationOptions[element.id] = element.name;
                    });

                    var justOne = (Object.keys(newLocationOptions).length == 1) ? true : false;

                    var $el = $("#location_id");

                    $el.empty();

                    if (!justOne) {
                        $el.append($("<option></option>")
                            .attr("value", null).text("Choose location"));
                    } else {
                        $el.append($("<option></option>")
                            .attr("value", null).attr("selected", "selected").text("Choose location"));
                    }

                    $.each(newLocationOptions, function (key, value) {
                        if (justOne) {
                            $el.append($("<option></option>")
                                .attr("value", key).attr("selected", "selected").text(value));
                        } else {
                            $el.append($("<option></option>")
                                .attr("value", key).text(value));
                        }
                    });
                }

            });

        }

        createOptionsOnLocationAndUserByChoosingOrganization();

        $("#organization_id").change(function () {
            createOptionsOnLocationAndUserByChoosingOrganization();
        });

        $('#inventoryEditForm').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                name: {
                    message: 'The name is not valid',
                    validators: {
                        notEmpty: {
                            message: 'The name is required and cannot be empty'
                        },
                        stringLength: {
                            min: 3,
                            max: 50,
                            message: 'The name must be more than 3 and less than 50 characters long'
                        }
                    }
                },
                number: {
                    validators: {
                        notEmpty: {
                            message: 'The number is required and cannot be empty'
                        },
                        callback: {
                            message: 'This number has already existed',
                            callback: function (value, validator) {

                                if (value == true) {
                                    return true;
                                }

                                var isValid = true;

                                $.ajax({
                                    url: isNumberValidCheckURL,
                                    dataType: 'json',
                                    async: false,
                                    data: {id: id, number: value},
                                    success: function (json) {
                                        isValid = json["isValid"];
                                    }
                                });

                                if(isValid){
                                    return true;
                                }else{
                                    return false;
                                }
                            }
                        }
                    }
                },
                status: {
                    validators: {
                        notEmpty: {
                            message: 'The status is required and cannot be empty'
                        }
                    }
                }
            }
        });

    });

})(jQuery, window, document);
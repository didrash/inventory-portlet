(function ($, W, D) {

    $(D).ready(function () {

        $('#statusEditForm').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                name: {
                    message: 'The name is not valid',
                    validators: {
                        notEmpty: {
                            message: 'The name is required and cannot be empty'
                        },
                        stringLength: {
                            min: 3,
                            max: 50,
                            message: 'The name must be more than 3 and less than 50 characters long'
                        },
                        callback: {
                            message: 'This name has already existed',
                            callback: function (value, validator) {

                                if (value == true) {
                                    return true;
                                }

                                var isValid = true;

                                $.ajax({
                                    url: isNameValidCheckURL,
                                    dataType: 'json',
                                    async: false,
                                    data: {id: id, name: value},
                                    success: function (json) {
                                        isValid = json["isValid"];
                                    }
                                });

                                if (isValid) {
                                    return true;
                                } else {
                                    return false;
                                }
                            }
                        }

                    }
                }
            }
        });

    });

})(jQuery, window, document);
